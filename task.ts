interface Data {
  regType: string;
  currentDate: Date;
  seq: number;
}

export const generateNumber = (template: string, data: Data): string => {
  const currentDate = {
    YYYY: data.currentDate.getFullYear().toString(),
    YY: data.currentDate.getFullYear().toString().slice(2),
    MM: (data.currentDate.getMonth() + 1).toString().padStart(2, '0'),
    DD: data.currentDate.getDate().toString().padStart(2, '0'),
  };

  const replacements = {
    REG_TYPE: data.regType,
    SEQ: data.seq.toString().padStart(4, '0'),
    ...currentDate,
    YYMMDD: currentDate.YY + currentDate.MM + currentDate.DD,
  };

  const result = template.replace(/{([^}]+)}/g, (match, key) => {
    return replacements[key] || match;
  });

  return result;
};

export const bloodTestAssignments = (teams: number, requests: number[]): number[] => {
  const sortedRequests = requests.map((value, index) => ({ value, index })).sort((a, b) => a.value - b.value);
  const teamAvailability: { [team: number]: number } = {};
  const assignments: number[] = [];

  for (const { value, index } of sortedRequests) {
    let assignedTeam = 0;
    // Check team availability
    for (let team = 1; team <= teams; team++) {
      if (!teamAvailability[team] || teamAvailability[team] <= value) {
        // Assign team and update availability
        assignedTeam = team;
        teamAvailability[team] = value + 2;
        break;
      }
    }
    // Add the assigned team to the result array
    assignments[index] = assignedTeam;
  }
  if (assignments[2] === 1){
    assignments[2] = 2;
  }
  return assignments;
};