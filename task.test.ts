import { generateNumber } from './task';
import { bloodTestAssignments } from './task';

describe('Task 1', () => {
  test('Test Case 1', () => {
    const data = {
      regType: 'RI',
      currentDate: new Date('2023-01-01'),
      seq: 1,
    };
    const template = '{SEQ}-{YYYY}/{MM}/{DD}-{REG_TYPE}';
    const result = generateNumber(template, data);
    expect(result).toBe('0001-2023/01/01-RI');
  });

  test('Test Case 2', () => {
    const data = {
      regType: 'RI',
      currentDate: new Date('2023-01-01'),
      seq: 1,
    };
    const template = '{REG_TYPE}/{YYMMDD}/{SEQ}';
    const result = generateNumber(template, data);
    expect(result).toBe('RI/230101/0001');
  });
});

describe('Task 2', () => {
  test('test case 1', () => {
    const requests = [9, 10, 13, 10, 11];
    const teams = 3;
    const assignments = bloodTestAssignments(teams, requests);
    expect(assignments).toEqual([1, 2, 2, 3, 1]);
  });

  test('test case 2', () => {
    const requests = [9, 10, 10, 10, 11];
    const teams = 3;
    const assignments = bloodTestAssignments(teams, requests);
    expect(assignments).toEqual([1, 2, 3, 0, 1]);
  });
});
